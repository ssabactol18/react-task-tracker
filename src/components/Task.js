import { FaTimes } from 'react-icons/fa'

const Task = ({ task, onDelete, onToggle }) => {
    return (
        <div className={`task ${task.reminder ? 'reminder' : ''}`} onDoubleClick={() => onToggle(task.id)}>
            <h3>{task.text} <small><FaTimes style={iconFaTimesStyle} onClick={() => onDelete(task.id)} /></small></h3>
            <p>{task.day}</p>
        </div>
    )
}

const iconFaTimesStyle = {
    color: 'red',
    cursor: 'pointer'
}

export default Task
